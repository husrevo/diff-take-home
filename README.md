# take-home-test-diff

This project is a take home test and the requirements and API design doesn't need to make sense :)  

## Running and building

Just like any Play application 

`sbt run` to run in test mode, `sbt dist` to build. If I had time and added dockerisation that would be much cooler to 
run a single command to get an instance of cassandra and API to launch and start working. 

## Current situation

There's an in-memory data store for test purposes. Check `conf/application.conf` for `app.databaseType`. It's `test` for 
testing purposes.   
   
You can make it run against a Cassandra cluster by changing it to `cassandra` and modifying configuration right below.   

To get a single instance dockerised Cassandra simply run: 

`docker run --name cassandra-phantom -d -e CASSANDRA_BROADCAST_ADDRESS=localhost -p 9042:9042 cassandra:3.0`  

Then change app.databaseType configuration to "cassandra".   
Then you can run all tests with:   
`sbt test` 

Please note that app will create necessary keyspace and the table itself in if they do not exist. 

## Further improvements

I would like to put a docker-compose.yml that would run cassandra and API alongside. Building a docker image for a Play 
app is almost trivial with sbt-native-packager and the cassandra connection configuration in the API can be overridden 
with environment variables within docker-compose.yml

## Play and DI

Most of my time was spent around refreshing my knowledge with Play framework. The problem is that the DI etc has changed 
quite a lot from last time I used it. Especially because I've pulled this seed project `play-scala-compile-di-example` 
for being able to plug different implementations of DataStore if in test or prod, which I ended up not using. At some 
point I decided to deliver something rather than getting lost in the search of how to do it the best way in Play. 

## About the delay

To be honest, the take home exercise was a bit boring, though I accept it is the most realistic one I have had for a 
while. The reason it felt boring and made me procrastinate it was because weekends and personal time is for quick hacks,
proof of ideas, taking challenges rather than doing stuff properly, writing tests and integration tests. Though I 
understand how this is more useful to see for an employer. 

## The criticism of the API design

The service is supposed to compare two strings and the API design given for the test is obviously designed to evaluate 
coding and technical choices. In a real world example, instead of getting two strings in two separate requests and 
saving them to return the diff with another request wouldn't be the best approach because it creates a bunch of 
unnecessary complications. On of them is the necessity of storing these values.   
   
If the problem with providing both values to get back the diff each time is that your strings are quite large and you 
compare them quite frequently, I would suggest providing a put method that creates UUIDs for text uploaded and return 
it. After uploading both part you can just reuse their UUIDs to call another API providing both UUIDs to the texts you 
want to compare.   

But it's clear that the reason to have such an API is to test the persistence choices, error handling, testing external 
components etc. 

## Design Decisions

### Key Value Store
To store the inputs, using a key value store like Cassandra sounds like a good idea. Given their ability so partition 
for linear scalability and replication for high availability and fault tolerance they are a good fit for this kind of 
storage needs.

### Partition Key
`diffID` is a natural partition key for us. Because concurrency across different `diffIDs` is not necessary. And our 
workload scales quite parallel to the amount of `diffID` we have. So it's a good idea to partition based on that.

Keeping `side` (left or right) out of partition key is another important decision. We want to be able to get both sides 
from the same node. Since we can't give a meaningful response if we can't get both of the sides, it makes sense to keep 
them in the same partition so they either will be both available or both unreachable in case of a disaster.

### Primary Key
`(diffID, side)` pair will be our primary key, since we don't want to overwrite anything with same diffId and side. 