import controllers.Differ
import model._
import org.scalatest.{FlatSpec, Matchers}

import scala.util.Random

class DifferSpec extends FlatSpec with Matchers {

  "A differ" should "return SizeDoNotMatch when two strings have different sizes" in {
    val fortyTwoAs = "A"*42
    val fourTwentyBs = "B"*420
    Differ.diff(fortyTwoAs, fourTwentyBs) shouldBe SizeDoNotMatch
  }

  it should "return Equals when two strings are same" in {
    val original = randString()
    val clone = new String(original)
    Differ.diff(original, clone) shouldBe Equals
  }

  it should "return Equals when both strings are empty" in {
    Differ.diff("", "") shouldBe Equals
  }

  it should "find differences when inputs start differently" in {
    val original = randString()
    val cloneA = "A"*10 + original
    val cloneB = "B"*10 + original
    Differ.diff(cloneA, cloneB) shouldBe ContentDoNotMatch(List(Difference(0,10)))
  }

  it should "find differences when inputs end differently" in {
    val original = randString()
    val differenceSize = 1 + Random.nextInt(100)
    val cloneA = original + "A" * differenceSize
    val cloneB = original + "B" * differenceSize
    Differ.diff(cloneA, cloneB) shouldBe ContentDoNotMatch(List(Difference(offset = original.length, length = differenceSize)))
  }

  it should "find out it if all the characters are different" in {
    val size = 1 + Random.nextInt(100)
    Differ.diff("A"*size, "B"*size) shouldBe ContentDoNotMatch(List(Difference(offset = 0, length = size)))
  }

  it should "find differences when inputs are randomly different" in {
    for( _ <- 1 to 100) {
      var aSoFar = Random.nextString(Random.nextInt(2))
      var bSoFar = new String(aSoFar)
      val diffsToCreate = 1+Random.nextInt(5)
      val differences = for(i <- 1 to diffsToCreate) yield {
        val currentIndex = aSoFar.length
        val mismatchingChars = 1 + Random.nextInt(100) // at least 1 different char
        val nextMatchingChars =
          if(i == diffsToCreate) // if end of string may be end differently by adding 0 matching chars
            randString(max = 2)
          else // make sure you have at least one common char between two differences
            randString(max = 10, min = 1)
        aSoFar = aSoFar + "A" * mismatchingChars + nextMatchingChars
        bSoFar = bSoFar + "B" * mismatchingChars + nextMatchingChars
        Difference(offset = currentIndex, length = mismatchingChars)
      }
        Differ.diff(aSoFar, bSoFar) shouldBe ContentDoNotMatch(differences.toList)
    }
  }

  def randString(max : Int = 100, min : Int = 0): String = Random.nextString(Random.nextInt(max-min)+min)
}
