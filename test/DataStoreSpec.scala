import controllers.{DataNotFound, DataStore}
import model.{Input, Inputs}
import org.scalatest.concurrent.Futures
import org.scalatest.{AsyncFlatSpec, BeforeAndAfterAll, Matchers}

import scala.util.Random

trait DataStoreSpec extends Matchers with Futures with BeforeAndAfterAll { this: AsyncFlatSpec =>
  def dataStore: DataStore

  override def afterAll(): Unit = {
    dataStore.clean()
  }

  def putAndGetKeyValueDataStore() = {
    it should "return the item we put" in {
      val randomInput = randInput
      dataStore.put(randomInput).value.flatMap { done =>
        done.isRight shouldBe true
        dataStore.get(randomInput.diffId, randomInput.side).value.map { either =>
          either shouldBe Right(randomInput.input)
        }
      }
    }

    it should "return DataNotFound when we call getBoth for ID and have only one side" in {
      val randomInput = randInput
      dataStore.put(randomInput).value.flatMap { done =>
        done.isRight shouldBe true
        dataStore.getBoth(randomInput.diffId).value.map { either =>
          either shouldBe Left(DataNotFound)
        }
      }
    }

    it should "return both strings when we call getBoth have an ID after putting both sides" in {
      val diffId = randID
      val randomLeft = Input(diffId, model.Left, randText)
      val randomRight = Input(diffId, model.Right, randText)
      dataStore.put(randomLeft).value.flatMap { leftDone =>
        leftDone.isRight shouldBe true
        dataStore.put(randomRight).value.flatMap { rightDone =>
          rightDone.isRight shouldBe true
          dataStore.getBoth(diffId).value.map { either =>
            either shouldBe Right(Inputs(diffId, randomLeft.input, randomRight.input))
          }
        }
      }
    }
  }

  def randID = Random.nextInt(1000).toString
  def randSide = if(Random.nextBoolean) model.Left else model.Right
  def randText = Random.alphanumeric.take(1+Random.nextInt(1000)).mkString
  def randInput = Input(randID, randSide, randText)

}
