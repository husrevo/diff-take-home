import model.ContentDoNotMatch
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play._
import play.api.http.ContentTypes
import play.api.libs.json.{JsNull, JsString, JsValue, Json}
import play.api.mvc.{AnyContentAsJson, Headers}
import play.api.test.Helpers._
import play.api.test._

/**
 * Runs an integration test with an application
 */
class ApplicationSpec extends PlaySpec
  with BaseOneAppPerTest
  with MyApplicationFactory
  with ScalaFutures {

  "Routes" should {
    "send 404 on a bad request" in {
      route(app, FakeRequest(GET, "/boum")).map(status(_)) mustBe Some(NOT_FOUND)
    }
  }

  "Differ API" should {
    "return 404 when nothing was put" in {
      val diff1 = getRequest("1").get
      status(diff1) mustBe NOT_FOUND
    }
    "return 201 Created when left side is put" in {
      val putLeft1 = putRequest("1", "left", JsString("AAAAAA==")).get
      status(putLeft1) mustBe CREATED
    }
    "still return 404 when the right is missing" in {
      val diff1 = getRequest("1").get
      status(diff1) mustBe NOT_FOUND
    }
    "return 201 Created when right side is put" in {
      val putRight1 = putRequest("1", "right", JsString("AAAAAA==")).get
      status(putRight1) mustBe CREATED
    }
    "return 200 OK with Equals when both values are the same" in {
      val diff1 = getRequest("1").get
      status(diff1) mustBe OK
      contentType(diff1) mustBe Some(ContentTypes.JSON)
      contentAsJson(diff1) mustBe Json.parse(
        """
          |{
          |  "diffResultType": "Equals"
          |}
        """.stripMargin)
    }
    "return 201 Created when right side is overwritten" in {
      val putRight1 = putRequest("1", "right", JsString("QAQQAA==")).get
      status(putRight1) mustBe CREATED
    }
    "return 200 OK with appropriate diff output when both values present and different" in {
      val diff1 = getRequest("1").get
      status(diff1) mustBe OK
      contentType(diff1) mustBe Some(ContentTypes.JSON)
      contentAsJson(diff1) mustBe Json.parse(
        """
          |{
          |  "diffResultType": "ContentDoNotMatch",
          |  "diffs": [
          |    {
          |      "offset": 0,
          |      "length": 1
          |    },
          |    {
          |      "offset": 2,
          |      "length": 2
          |    }
          |  ]
          |}
        """.stripMargin)
    }

    "return 201 Created when right side is overwritten with something shorter" in {
      val putRight1 = putRequest("1", "right", JsString("AAA=")).get
      status(putRight1) mustBe CREATED
    }

    "return 200 OK with SizeDoNotMatch when values are different in size" in {
      val diff1 = getRequest("1").get
      status(diff1) mustBe OK
      contentType(diff1) mustBe Some(ContentTypes.JSON)
      contentAsJson(diff1) mustBe Json.parse(
        """
          |{
          |  "diffResultType": "SizeDoNotMatch"
          |}
        """.stripMargin)
    }



    "return 400 Bad Request when the data is null" in {
      val putRight1 = putRequest("1", "right", JsNull).get
      status(putRight1) mustBe BAD_REQUEST
    }

    def getRequest(diffId: String) = {
      route(app, FakeRequest(GET, s"/v1/diff/$diffId"))
    }

    def putRequest(diffId: String, side: String, data: JsValue) = {
      route(app, FakeRequest(PUT, s"/v1/diff/$diffId/$side", headers = Headers().add("content-type" -> "application/json"), body = Json.obj("data" -> data)))
    }
  }
}
