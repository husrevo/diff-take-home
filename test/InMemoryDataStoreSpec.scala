import controllers.{DataStore, InMemoryDataStore}
import org.scalatest.AsyncFlatSpec

class InMemoryDataStoreSpec extends AsyncFlatSpec with DataStoreSpec {
  override def dataStore: DataStore = InMemoryDataStore

  "An InMemoryDataStore" should behave like(putAndGetKeyValueDataStore)
}
