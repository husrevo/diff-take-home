package controllers

import javax.inject.Inject

import cats.implicits._
import model._
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class HomeController @Inject()(cc: ControllerComponents, dataStore: DataStore) extends AbstractController(cc) {

  def put(id: String, side: Side) = Action(parse.json).async { implicit req =>
    (req.body \ "data").asOpt[String].map { data =>
      dataStore.put(Input(id, side, data)).map { _ =>
        Created("")
      }.leftMap( error => InternalServerError(error.exception.getLocalizedMessage)).merge
    }.getOrElse(Future.successful(BadRequest("field data should exist and not be null")))
  }

  def diff(id: String) = Action.async {
    dataStore.getBoth(id).map { inputs: Inputs =>
      val diff = Differ.diff(inputs.left, inputs.right)
      Ok(Json.toJson(diff))
    }.leftMap {
      case DbError(ex) => InternalServerError(ex.getLocalizedMessage)
      case DataNotFound => NotFound("Both sides should be uploaded to get a diff")
    }.merge
  }

  def putLeft(id: String) = put(id, Left)
  def putRight(id: String) = put(id, Right)

}
