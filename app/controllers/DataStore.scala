package controllers

import cats.data.EitherT
import cats.implicits._
import model.{Input, Inputs, Side}

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

sealed trait Error
case class DbError(exception: Exception) extends Error
case object DataNotFound extends Error


trait DataStore {
  def put(input: model.Input) : EitherT[Future, DbError, Unit]
  def get(diffId: String, side: Side) : EitherT[Future, Error, String]
  def clean() : EitherT[Future, DbError, Unit]

  def getBoth(diffId: String) : EitherT[Future, Error, Inputs] = {
    for{
      left <- get(diffId, model.Left)
      right <- get(diffId, model.Right)
    } yield Inputs(diffId, left, right)
  }
}

object InMemoryDataStore extends DataStore{
  val state = new mutable.HashMap[(String, Side), String]()
  override def put(input: Input): EitherT[Future, DbError, Unit] = {
    state.put((input.diffId, input.side), input.input)
    EitherT.liftT(Future.successful(()))
  }

  override def get(diffId: String, side: Side): EitherT[Future, Error, String] = {
    val eitherDataOrError: Either[Error, String] = state.get((diffId, side)).toRight(DataNotFound)
    EitherT.fromEither(eitherDataOrError)
  }

  override def clean() : EitherT[Future, DbError, Unit] = {
    state.clear()
    EitherT.liftT(Future.successful(()))
  }
}
