package controllers

import model._

object Differ {

  def diff(left: String, right: String) : DiffResult = {
    if(left.length == right.length) {
      val diffs = diffOfSameLengthStrings(left, right)
      if(diffs.isEmpty)
        Equals
      else
        ContentDoNotMatch(diffs)
    } else SizeDoNotMatch
  }

  private def diffOfSameLengthStrings(left: String, right: String) : List[Difference] = {
    case class Acc(lastDiffStart: Option[Int], allDifferences: List[Difference])
    val equalities = left.zip(right).map {
      case (leftChar, rightChar) =>
        leftChar == rightChar
    }

    val Acc(lastStart, differences) = equalities.zipWithIndex.foldLeft(Acc(None, List.empty)) {
      case (Acc(None, prevList), (false, index)) =>
        Acc(lastDiffStart = Some(index), prevList)
      case (Acc(Some(start), prevList), (true, end)) =>
        Acc(lastDiffStart = None, Difference(start, end-start) :: prevList)
      case (acc, _) => acc
    }

    val allDiffs = lastStart match {
      case None => differences
      case Some(start) => Difference(start, left.length-start) :: differences
    }

    allDiffs.reverse
  }

}
