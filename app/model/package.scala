import play.api.libs.json.{JsValue, Json, OWrites, Writes}

package object model {
  sealed trait Side
  case object Left extends Side
  case object Right extends Side

  case class Input(diffId: String, side: Side, input: String)
  case class Inputs(diffId: String, left: String, right: String)
  case class Difference(offset: Int, length: Int)

  trait DiffResult {
    val diffResultType: String = toString
  }
  case object SizeDoNotMatch extends DiffResult
  case class ContentDoNotMatch(diffs: List[Difference]) extends DiffResult {
    override val diffResultType = "ContentDoNotMatch"
  }
  case object Equals extends DiffResult

  object DiffResult {
    implicit val difference = Json.writes[Difference]

    implicit val diffResultWrites = new Writes[DiffResult] {
      override def writes(o: DiffResult): JsValue = o match {
        case c : ContentDoNotMatch =>
          // Play's auto json generation skips the vals that are not a part of the case class constructor.
          Json.obj("diffResultType" -> c.diffResultType, "diffs" -> Json.toJson(c.diffs))
        case other : DiffResult =>
          Json.obj("diffResultType" -> other.diffResultType)
      }
    }
  }

}
