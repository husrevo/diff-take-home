


import controllers.InMemoryDataStore
import db.{CassandraConfig, CassandraConnector, CassandraDataStore}
import play.api.{Application, ApplicationLoader, BuiltInComponentsFromContext}
import play.api.routing.Router

class MyApplicationLoader extends ApplicationLoader {
  private var components: MyComponents = _

  def load(context: ApplicationLoader.Context): Application = {
    components = new MyComponents(context)
    components.application
  }
}

class MyComponents(context: ApplicationLoader.Context) 
  extends BuiltInComponentsFromContext(context)
  with play.filters.HttpFiltersComponents {


  lazy private val config = context.initialConfiguration
  lazy private val dataStore = config.get[String]("app.databaseType") match {
    case "test" => InMemoryDataStore
    case "cassandra" =>
      val cassandraConfig = config.underlying.getConfig("app.cassandra")
      val host = cassandraConfig.getString("host").split(',')
      val port = cassandraConfig.getInt("port")
      val keyspace = cassandraConfig.getString("keyspace")
      val replicationFactor = cassandraConfig.getInt("replicationFactor")
      val connectionConf = CassandraConfig(host, port, keyspace, replicationFactor)
      val connection = new CassandraConnector(connectionConf)
      new CassandraDataStore(connection.connector)
  }

  lazy val homeController = new _root_.controllers.HomeController(controllerComponents, dataStore)

  lazy val router: Router = new _root_.router.Routes(httpErrorHandler, homeController)
}
