package db

import com.outworkers.phantom.connectors.{ContactPoint, KeySpaceCQLQuery}
import com.outworkers.phantom.dsl._

case class CassandraConfig(host: Seq[String], port: Int, keyspace: String, replicationFactor: Int)

class CassandraConnector(config : CassandraConfig) {

  private[this] val space = KeySpace(config.keyspace).builder
    .ifNotExists()
    .`with`(replication eqs SimpleStrategy.replication_factor(config.replicationFactor))
    .and(durable_writes eqs true)

  private[this] val ks = new KeySpaceCQLQuery {
    override def keyspace: String = space.keySpace.name
    override def queryString: String = space.queryString
  }

  lazy val connector: CassandraConnection = ContactPoints(config.host, config.port).keySpace(ks)
}