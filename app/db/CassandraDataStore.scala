package db

import cats.data.EitherT
import cats.implicits._
import com.outworkers.phantom.dsl.{ResultSet, _}
import controllers._
import model.{Input, Side}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

class CassandraNotExecutedException(results : Seq[ResultSet]) extends Exception

class CassandraDataStore(override val connector: CassandraConnection)
  extends Database[CassandraDataStore](connector)
  with DataStore {
  object users extends Inputs with Connector
  Await.ready(this.createAsync(), 10 seconds)

  override def put(input: Input) = EitherT(users.insert(input).map(rs => executedSuccessfully(Seq(rs))))

  override def get(diffId: String, side: Side) = EitherT(users.getByIdAndSide(diffId, side).map(_.toRight(DataNotFound)))

  override def clean(): EitherT[Future, DbError, Unit] = {
    for{
      _ <- EitherT(this.truncateAsync().map(executedSuccessfully))
      _ <- EitherT(this.createAsync().map(executedSuccessfully))
    } yield ()
  }

  def executedSuccessfully(results : Seq[ResultSet]) : Either[DbError, Unit] = {
    if(results.forall(_.wasApplied()))
      Right(())
    else
      Left(DbError(new CassandraNotExecutedException(results.filterNot(_.wasApplied))))
  }
}

