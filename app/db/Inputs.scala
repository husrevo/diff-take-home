package db

import com.outworkers.phantom.dsl._
import com.outworkers.phantom.Table
import com.outworkers.phantom.keys.{PartitionKey, PrimaryKey}
import model.{Input, Side}

import scala.concurrent.Future

abstract class Inputs extends Table[Inputs, Input] {
  object diffId extends StringColumn with PartitionKey
  object side extends StringColumn with PrimaryKey
  object input extends StringColumn

  private[this] lazy val getByIdAndSideStatement = select(_.input)
    .where(_.diffId eqs ?)
    .and(_.side eqs ?)
    .prepareAsync()

  private[this] lazy val storeInputStatement = insert()
    .p_value(_.diffId,?)
    .p_value(_.side, ?)
    .p_value(_.input, ?)
    .prepareAsync()

  def getByIdAndSide(id: String, side : Side): Future[Option[String]] = {
    getByIdAndSideStatement.flatMap(_.bind(id, side.toString).one())
  }
  def insert(input: Input) : Future[ResultSet] = {
    storeInputStatement.flatMap(_.bind(input.diffId, input.side.toString, input.input).future())
  }
}